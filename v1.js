const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db/getData');


const v1 = express.Router();

v1.use(bodyParser.json());

v1.get('/', (req, res) => {
  let result = {categories: null, items: null};
  db.getAllItems((err, rows) => {
    result.items = rows;
    db.getAllCategories((err, rowz) => {
      result.categories = rowz;
      res.json(result);
    });
  });
});

v1.post('/', (req, res) => {
  console.log('vino la sicodelia');
  console.info('req.body');
  console.log(req.body);
  db.addItem(req.body);
  res.json(req.body);
});

module.exports = v1;
