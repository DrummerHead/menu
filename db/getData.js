const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(path.join(__dirname, 'main.db'));

const getAllItems = (fn) =>
  db.serialize(() => {
    db.all(`
SELECT rowid AS id, *
FROM item;`
      , fn);
  });

const getAllCategories = (fn) =>
  db.serialize(() => {
    db.all(`
SELECT *
FROM category;`
      , fn);
  });

const addItem = (item) => {
  console.info('item inside func');
  console.log(item);
  db.run(`
INSERT INTO item
(title, description, photo, price, category, vegan, vegetarian, spiceLevel)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
  `, item.title, null, null, item.price, null, null, null, null);
};

module.exports = { getAllItems, getAllCategories, addItem };
