const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('main.db');

const itemSample = require('./item-sample');
const categorySample = require('./category-sample');

// Create item and category tables
//
db.serialize(() => {
  db.run(`
CREATE TABLE IF NOT EXISTS item (
  item_id INTEGER PRIMARY KEY,
  title TEXT NOT NULL,
  description TEXT,
  photo TEXT,
  price INTEGER NOT NULL,
  currency TEXT,
  vegan INTEGER,
  vegetarian INTEGER,
  spiceLevel INTEGER,
  category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES category(category_id)
);
  `);

  db.run(`
CREATE TABLE IF NOT EXISTS category (
  category_id INTEGER PRIMARY KEY,
  title TEXT NOT NULL
);
  `);

  // Populate these tables with sample data
  //
  const itemStmt = db.prepare(`
INSERT INTO item
(title, description, photo, price, currency, vegan, vegetarian, spiceLevel, category_id)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
  `);
  for (const item of itemSample) {
    itemStmt.run(item.title, item.description, item.photo, item.price, item.currency, (item.vegan ? 1 : 0), (item.vegetarian ? 1 : 0), item.spiceLevel, item.category_id);
  }
  itemStmt.finalize();

  const catStmt = db.prepare(`
INSERT INTO category
(category_id, title)
VALUES (?, ?)
  `);
  for (const category of categorySample) {
    catStmt.run(category.category_id, category.title);
  }
  catStmt.finalize();
});

db.close();