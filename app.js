const express = require('express');
const morgan = require('morgan');
const v1 = require('./v1');

const app = express();

app.set('port', process.env.PORT || 3001);
app.use(morgan('short'));

app.use('/v1', v1);
app.use(express.static('public'));


app.listen(app.get('port'), () => {
  console.log(`Server started on port ${app.get('port')}`);
});
